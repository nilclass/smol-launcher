use std::io::prelude::*;
use std::os::unix::io::AsRawFd;
use nix::poll::{poll, PollFd, PollFlags};
use memmap::MmapOptions;
use nix::sys::timerfd::{TimerFd, ClockId, TimerFlags, Expiration, TimerSetTimeFlags};
use nix::sys::time::{TimeSpec, TimeValLike};
use serde::Deserialize;

use wayland_client::{
    Display,
    GlobalManager,
    Main,
    protocol::{
        wl_seat::{WlSeat, Event as SeatEvent, Capability},
        wl_compositor::WlCompositor,
        wl_surface::WlSurface,
        wl_output::{WlOutput, Event as OutputEvent, Mode as OutputMode},
        wl_buffer::WlBuffer,
        wl_shm::{WlShm, Format as ShmFormat, Event as ShmEvent},
        wl_touch::{WlTouch, Event as TouchEvent},
        wl_pointer::{WlPointer, Event as PointerEvent, ButtonState},
        wl_callback::{WlCallback, Event as CallbackEvent},
    },
};
use wayland_protocols::wlr::unstable::layer_shell::v1::client::{
    zwlr_layer_shell_v1::{ZwlrLayerShellV1, Layer},
    zwlr_layer_surface_v1::{ZwlrLayerSurfaceV1, Event as LayerSurfaceEvent},
};

macro_rules! app_callback {
    ($callback: ident) => {
        |target, event, mut app| app.get::<App>().unwrap(). $callback (target, event)
    };
}

#[derive(Deserialize)]
struct LauncherItemConfig {
    label: String,
    icon: String,
    command: Vec<String>,
}

impl LauncherItemConfig {
    fn to_item(&self) -> LauncherItem {
        LauncherItem {
            label: self.label.clone(),
            command: self.command.clone(),
            icon: load_icon(&self.icon),
        }
    }
}

#[derive(Deserialize)]
struct Config {
    clock_update_interval: i64,
    time_format: String,
    clock_font_size: f32,
    launcher_font_size: f32,
    font: String,
    launcher: Vec<LauncherItemConfig>,
    launcher_offset: f64,
    icon_size: f64,
    h_spacing: f64,
    v_spacing: f64,
    background_color: [f64; 3],
    text_color: [f64; 3],
}

struct LauncherItem {
    label: String,
    icon: cairo::ImageSurface,
    command: Vec<String>,
}

struct App<'a> {
    config: Config,
    surface: Main<WlSurface>,
    shm: Main<WlShm>,
    size: Option<(u32, u32)>,
    output_scale: i32,
    format: Option<ShmFormat>,
    pool_file: Option<std::fs::File>,
    buffers: Option<[Main<WlBuffer>; 2]>,
    current_buffer: u32,
    frame_scheduled: bool,
    press_point: Option<(f64, f64)>,

    launcher_items: Vec<LauncherItem>,
    font: rusttype::Font<'a>,

    // pointer-internal state:
    pointer_position: Option<(f64, f64)>,
    pointer_button_down: bool,

    // touch-internal state
    touch_state: Option<i32>,
}

impl<'a> App<'a> {
    fn new(config: Config, surface: Main<WlSurface>, shm: Main<WlShm>) -> Self {
        let mut font_buf = vec![];
        std::fs::File::open(&config.font)
            .expect(&format!("Failed to open font at {}", config.font))
            .read_to_end(&mut font_buf)
            .expect(&format!("Failed to read font data from {}", config.font));
        let font = rusttype::Font::try_from_vec(font_buf)
            .expect(&format!("Failed to parse font from {}", config.font));
        let launcher_items = config.launcher.iter().map(|launcher| launcher.to_item()).collect();
        Self {
            config,
            surface,
            shm,
            size: None,
            output_scale: 1,
            format: None,
            pool_file: None,
            buffers: None,
            current_buffer: 0,
            frame_scheduled: false,
            press_point: None,
            pointer_position: None,
            pointer_button_down: false,
            touch_state: None,
            launcher_items,
            font,
        }
    }

    fn launcher_item_layout(&self) -> impl std::iter::Iterator<Item = (&LauncherItem, (f64, f64), bool)> {
        let size = self.size.unwrap();
        let launcher_offset = self.config.launcher_offset;
        let icon_size = self.config.icon_size;
        let h_spacing = self.config.h_spacing;
        let v_spacing = self.config.v_spacing;
        let icons_per_row = ((size.0 as f64 - h_spacing) / (icon_size + h_spacing)).floor() as usize;
        self.launcher_items.iter().enumerate().map(move |(i, item)| {
            let row = (i / icons_per_row) as f64;
            let column = (i % icons_per_row) as f64;
            let offset = (
                column * (icon_size + h_spacing) + h_spacing,
                launcher_offset + row * (icon_size + v_spacing) + v_spacing
            );
            let mut is_pressed = false;
            if let Some(pos) = self.press_point {
                if pos.0 >= offset.0 && pos.0 <= offset.0 + icon_size && pos.1 >= offset.1 && pos.1 <= offset.1 + icon_size {
                    is_pressed = true;
                }
            }
            (item, offset, is_pressed)
        })
    }

    fn draw(&self, ctx: cairo::Context) {
        let size = self.size.unwrap();
        let bg = self.config.background_color;

        // Background:
        ctx.save();
        ctx.set_source_rgb(bg[0], bg[1], bg[2]);
        ctx.set_operator(cairo::Operator::Source);
        ctx.paint();
        ctx.restore();

        // Clock:
        ctx.save();
        let text = chrono::Local::now().format(&self.config.time_format).to_string();
        self.draw_text(&ctx, &text, self.config.clock_font_size,
                       |width, height| (((size.0 as f64 - width) / 2.).floor(), (height as f64  * 1.5).floor()));

        let icon_size = self.config.icon_size;
        let h_spacing = self.config.h_spacing;

        // Launcher icons:
        for (item, offset, is_pressed) in self.launcher_item_layout() {
            if is_pressed {
                ctx.save();
                ctx.set_source_rgba(1., 0., 0., 0.5);
                ctx.rectangle(offset.0 - h_spacing / 2., offset.1 - h_spacing / 2., icon_size + h_spacing, icon_size + h_spacing);
                ctx.fill();
                ctx.restore();
            }

            ctx.save();
            ctx.translate(offset.0, offset.1);
            ctx.set_source_surface(&item.icon, 0., 0.);
            ctx.rectangle(0., 0., icon_size, icon_size);
            ctx.clip();
            ctx.paint();
            ctx.restore();

            self.draw_text(&ctx, &item.label, self.config.launcher_font_size, |width, _height| {
                ((offset.0 + icon_size / 2.) - width / 2., offset.1 + icon_size + 5.)
            });
        }
    }

    // Draw given text at specified size. The given `position` callback is called with the computed size of the text,
    // and is expected to return the top-left corner at which to start drawing the text.
    fn draw_text<F: FnOnce(f64, f64) -> (f64, f64)>(&self, ctx: &cairo::Context, text: &str, size: f32, position: F) {
        let color = self.config.text_color;
        ctx.save();
        let scale = rusttype::Scale::uniform(size);
        let layout = self.font.layout(text, scale, rusttype::point(0., 0.));
        let v_metrics = self.font.v_metrics(scale);
        let height = (v_metrics.ascent + v_metrics.descent).ceil() as f64;
        let width = layout.clone().last().unwrap().pixel_bounding_box().unwrap().max.x as f64;
        let offset = position(width, height);
        ctx.translate(offset.0, offset.1 + v_metrics.ascent as f64);
        for glyph in layout {
            if let Some(bb) = glyph.pixel_bounding_box() {
                glyph.draw(|x, y, v| {
                    let v = v as f64;
                    ctx.set_source_rgba(color[0], color[1], color[2], v);
                    ctx.rectangle(bb.min.x as f64 + x as f64, bb.min.y as f64 + y as f64, 1., 1.);
                    ctx.fill();
                });
            }
        }
        ctx.restore();
    }

    fn schedule_frame(&mut self) {
        if !self.frame_scheduled {
            let frame_callback = self.surface.frame();
            frame_callback.quick_assign(app_callback!(handle_frame_callback));
            self.surface.commit();
            self.frame_scheduled = true;
        }
    }

    fn render(&mut self) {
        let size = self.size.unwrap();
        let format = self.format.unwrap();
        let pool_file = self.pool_file.as_ref().unwrap();
        let bpp = bytes_per_pixel(format);

        let buffer_size = size.0 * size.1 * bpp as u32;
        let mmap = unsafe {
            MmapOptions::new()
                .len(buffer_size as usize)
                .offset((self.current_buffer * buffer_size) as u64)
                .map_mut(pool_file)
        }.unwrap();

        let cairo_surface = cairo::ImageSurface::create_for_data(
            mmap,
            shm_format_to_cairo(format).unwrap(),
            size.0 as i32,
            size.1 as i32,
            bpp * size.0 as i32,
        ).unwrap();

        self.draw(cairo::Context::new(&cairo_surface));

        self.surface.attach(Some(&self.buffers.as_ref().unwrap()[self.current_buffer as usize]), 0, 0);
        self.surface.set_buffer_scale(self.output_scale);
        self.surface.damage(0, 0, size.0 as i32, size.1 as i32);
        self.surface.commit();

        self.current_buffer = if self.current_buffer == 0 { 1 } else { 0 };
    }

    fn handle_frame_callback(&mut self, _callback: Main<WlCallback>, event: CallbackEvent) {
        match event {
            CallbackEvent::Done { callback_data: _ } => {
                self.frame_scheduled = false;
                self.render();
            }
            _ => {}
        }
    }

    fn handle_shm_event(&mut self, _shm: Main<WlShm>, event: ShmEvent) {
        match event {
            ShmEvent::Format { format } => {
                // Choose the first supported format:
                if self.format.is_none() && shm_format_to_cairo(format).is_some() {
                    self.format = Some(format);
                }
            }
            _ => {}
        }
    }

    fn handle_layer_surface_event(&mut self, layer_surface: Main<ZwlrLayerSurfaceV1>, event: LayerSurfaceEvent) {
        match event {
            LayerSurfaceEvent::Configure { serial, .. } => {
                let size = self.size.expect("Received 'configure' event before output mode is known!");
                layer_surface.set_size(size.0 / self.output_scale as u32, size.1 / self.output_scale as u32);
                layer_surface.ack_configure(serial);
                if self.pool_file.is_some() {
                    // clean up previous pool etc
                    self.pool_file.take();
                    for buffer in &self.buffers.take().unwrap() {
                        buffer.destroy();
                    }
                }

                let pool_size = size.0 * size.1 * 4 * 2;
                let pool_file = tempfile::tempfile().expect("tempfile");
                pool_file.set_len(pool_size as u64).unwrap();
                let shm_pool = self.shm.create_pool(pool_file.as_raw_fd(), pool_size as i32);
                self.pool_file = Some(pool_file);
                let format = self.format.unwrap();
                let bpp = bytes_per_pixel(format);
                let stride = bpp * size.0 as i32;
                let buffer0 = shm_pool.create_buffer(0, size.0 as i32, size.1 as i32, stride, format);
                let buffer1 = shm_pool.create_buffer(stride * size.1 as i32, size.0 as i32, size.1 as i32, stride, format);
                buffer0.quick_assign(|_, _, _| {});
                buffer1.quick_assign(|_, _, _| {});
                self.buffers = Some([buffer0, buffer1]);
                self.render();
                self.schedule_frame();
            }
            _ => {}
        }
    }

    fn handle_seat_event(&mut self, seat: Main<WlSeat>, event: SeatEvent) {
        match event {
            SeatEvent::Capabilities { capabilities } => {
                if capabilities.contains(Capability::Pointer) {
                    let wl_pointer = seat.get_pointer();
                    wl_pointer.quick_assign(app_callback!(handle_pointer_event));
                }

                if capabilities.contains(Capability::Touch) {
                    let wl_touch = seat.get_touch();
                    wl_touch.quick_assign(app_callback!(handle_touch_event));
                }
            }
            _ => {}
        }
    }

    fn handle_output_event(&mut self, _output: Main<WlOutput>, event: OutputEvent) {
        match event {
            OutputEvent::Mode { flags, width, height, refresh: _ } => {
                if flags.contains(OutputMode::Current) {
                    if self.size.is_some() {
                        // TODO: We could handle this in one of two ways:
                        //   1. Recreate the buffers (including shm pool etc) and resize the layer surface
                        //   2. Restart the application
                        println!("Mode changed: {:?} -> {}x{}. Not supported yet! Strange behaviour may occur.",
                                 self.size.unwrap(), width, height);
                    } else {
                        self.size = Some((width as u32, height as u32));
                    }
                }
            }
            OutputEvent::Scale { factor } => {
                self.output_scale = factor;
            }
            _ => {}
        }
    }

    fn execute(&self, item: &LauncherItem) {
        println!("Executing command {:?}", item.command);

        let exit_status = std::process::Command::new(&item.command[0])
            .args(&item.command[1..])
            .spawn()
            .expect("Failed to spawn process")
            .wait()
            .expect("Failed to wait for exit status");
        if !exit_status.success() {
            panic!("Child exited with non-zero exit status: {:?}", exit_status);
        }
    }

    fn handle_pointer_event(&mut self, _pointer: Main<WlPointer>, event: PointerEvent) {
        match event {
            PointerEvent::Motion { surface_x, surface_y, .. } => {
                self.pointer_position = Some((surface_x * self.output_scale as f64, surface_y * self.output_scale as f64));
                if self.pointer_button_down {
                    self.press_point = Some(self.pointer_position.unwrap());
                    self.schedule_frame();
                }
            }
            PointerEvent::Button { button, state, .. } => {
                if button == 0x110 /* BTN_LEFT */ {
                    match state {
                        ButtonState::Pressed => {
                            self.pointer_button_down = true;
                            self.press_point = Some(self.pointer_position.unwrap());
                        }
                        ButtonState::Released => {
                            if let Some((launcher, _, _)) = self.launcher_item_layout().find(|(_, _, is_pressed)| *is_pressed) {
                                self.execute(launcher);
                            }
                            self.pointer_button_down = false;
                            self.press_point = None;
                        }
                        _ => {}
                    }
                    self.schedule_frame();
                }
            }
            _ => {}
        }
    }

    fn handle_touch_event(&mut self, _touch: Main<WlTouch>, event: TouchEvent) {
        match event {
            TouchEvent::Down { id, x, y, .. } => {
                self.touch_state = Some(id);
                self.press_point = Some((x * self.output_scale as f64, y * self.output_scale as f64));
                self.schedule_frame();
            }
            TouchEvent::Motion { id, x, y, .. } => {
                if let Some(current_id) = self.touch_state {
                    if current_id == id {
                        self.press_point = Some((x * self.output_scale as f64, y * self.output_scale as f64));
                        self.schedule_frame();
                    }
                }
            }
            TouchEvent::Up { id, .. } => {
                if let Some(current_id) = self.touch_state {
                    if current_id == id {
                        if let Some((launcher, _, _)) = self.launcher_item_layout().find(|(_, _, is_pressed)| *is_pressed) {
                            self.execute(launcher);
                        }
                        self.touch_state = None;
                        self.press_point = None;
                        self.schedule_frame();
                    }
                }
            }
            _ => {}
        }
    }
}

fn shm_format_to_cairo(format: ShmFormat) -> Option<cairo::Format> {
    match format {
        ShmFormat::Argb8888 => Some(cairo::Format::ARgb32),
        ShmFormat::Xrgb8888 => Some(cairo::Format::Rgb24),
        ShmFormat::Xrgb2101010 => Some(cairo::Format::Rgb30),
        ShmFormat::Rgb565 => Some(cairo::Format::Rgb16_565),
        _ => None,
    }
}

fn bytes_per_pixel(format: ShmFormat) -> i32 {
    match format {
        ShmFormat::Argb8888 | ShmFormat::Xrgb8888 | ShmFormat::Xrgb2101010 => { 4 }
        ShmFormat::Rgb565 => { 2 }
        _ => { panic!("Color space is not supported: {:?}", format) }
    }
}

fn load_icon(path: &str) -> cairo::ImageSurface {
    cairo::ImageSurface::create_from_png(
        &mut std::fs::File::open(path).expect(&format!("Failed to open icon at {}", path))
    ).expect(&format!("Failed to read PNG data from {}", path))
}

fn main() {
    let mut config_path = std::path::PathBuf::from(
        std::env::vars().find(|(key, _)| key == "HOME").expect("HOME not set! Cannot locate config file.").1
    );
    config_path.push(".config/smol-launcher.yaml");
    let config_file = std::fs::File::open(&config_path)
        .expect(&format!("Failed to open config file at {}", config_path.to_str().unwrap()));
    let config = serde_yaml::from_reader::<_, Config>(&config_file)
        .expect(&format!("Failed to read config data from {}", config_path.to_str().unwrap()));

    let display = Display::connect_to_env()
        .expect("Failed to connect to display (connect_to_env). Check WAYLAND_DISPLAY and make sure the compositor is running.");
    let mut event_queue = display.create_event_queue();
    let attached_display = display.attach(event_queue.token());
    let global_manager = GlobalManager::new(&attached_display);
    event_queue.sync_roundtrip(&mut (), |_, _, _| {
        unreachable!();
    }).unwrap();

    let seat = global_manager.instantiate_exact::<WlSeat>(5).unwrap();
    let compositor = global_manager.instantiate_exact::<WlCompositor>(4).unwrap();
    let layer_shell = global_manager.instantiate_exact::<ZwlrLayerShellV1>(2).unwrap();
    let shm = global_manager.instantiate_exact::<WlShm>(1).unwrap();
    let output = global_manager.instantiate_range::<WlOutput>(2, 3).unwrap();

    let surface = compositor.create_surface();
    let layer_surface = layer_shell.get_layer_surface(&surface, Some(&output), Layer::Background, "smol-launcher".to_string());

    surface.commit();
    surface.quick_assign(|_, _, _| {});
    shm.quick_assign(app_callback!(handle_shm_event));
    layer_surface.quick_assign(app_callback!(handle_layer_surface_event));
    seat.quick_assign(app_callback!(handle_seat_event));
    output.quick_assign(app_callback!(handle_output_event));

    let mut app = App::new(config, surface, shm);

    let timer = TimerFd::new(ClockId::CLOCK_REALTIME, TimerFlags::empty()).unwrap();
    timer.set(Expiration::Interval(TimeSpec::microseconds(app.config.clock_update_interval * 1000)), TimerSetTimeFlags::empty()).unwrap();

    loop {
        if let Err(e) = display.flush() {
            if e.kind() != std::io::ErrorKind::WouldBlock {
                panic!("Error while trying to flush the wayland socket: {:?}", e);
            }
        }

        if let None = event_queue.prepare_read() {
            event_queue.dispatch_pending(&mut app, |event, _, _| {
                println!("Unhandled event: {:?}", event);
            }).expect("Dispatch failed");
            continue;
        }

        let display_socket_fd = display.get_connection_fd();

        let mut poll_fds = [
            PollFd::new(display_socket_fd, PollFlags::POLLIN),
            PollFd::new(timer.as_raw_fd(), PollFlags::POLLIN),
        ];

        if let Err(err) = poll(&mut poll_fds, -1) {
            panic!("Poll failed: {:?}", err);
        }

        if let Some(events) = poll_fds[0].revents() {
            // Input on wayland socket
            if events.contains(PollFlags::POLLIN) {
                if let Some(guard) = event_queue.prepare_read() {
                    if let Err(e) = guard.read_events() {
                        if e.kind() != std::io::ErrorKind::WouldBlock {
                            eprintln!("Error while reading events: {:?}", e);
                        }
                    }
                }

                event_queue.dispatch_pending(&mut app, |event, _, _| {
                    println!("Unhandled event: {:?}", event);
                }).expect("Dispatch failed");
            }
        }

        if let Some(events) = poll_fds[1].revents() {
            // Timerfd fired
            if events.contains(PollFlags::POLLIN) {
                timer.wait().unwrap();
                app.schedule_frame();
            }
        }
    }
}

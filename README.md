# smol-launcher

This is a small (~500 LOC) launcher application, intended to be used on linux based mobile devices running a wayland compositor.

It provides precisely two features:
- Display the current system time, with configurable format and precision
- Show a grid of launcher buttons, which each run a configured command

It should work on any wayland compositor that implements the `zwlr_layer_shell` protocol, such as `sway`.
In the future other shell protocols may be supported as well.

smol-launcher creates a surface on the `background` layer, so it will not work when another application displays a background as well (this includes backgrounds configured in the sway config via `output * bg ...`).

## Dependencies

smol-launcher is written in Rust, and depends on the following crates:
- `wayland-client`, `wayland-protocol`: Used to communicate with the wayland compositor
- `chrono`: Used to format the current time
- `rusttype`: Used to load fonts & draw text.
- `cairo-rs`: Used for general drawing, and loading PNGs. This may be replaced in the future, since it's a bit of an overkill.
- `nix`: Used to access UNIX APIs for `poll`, `timerfd`
- `memmap`: Provides a safe wrapper around `mmap` API

## Configuration

smol-launcher requires a configuration file located at `$HOME/.config/smol-launcher.yaml`.

A minimal configuration might look like this:
```
# Format of the displayed time.
# Documentation: https://docs.rs/chrono/0.4.19/chrono/format/strftime/index.html
#
# Examples:
#   %H:%M         23:59
#   %H:%M:%S      23:59:59
#   %H:%M:%S%.3f  23:59:59.999
#
time_format: "%H:%M"

# Base colors (RGB)
background_color: [0.0, 0.0, 0.0]
text_color: [1.0, 1.0, 1.0]

# How often to update the current time (in milliseconds). Make sure to adjust this
# when you modify time_format. For example, when only displaying the current hour,
# it's not necessary to refresh every minute. Similarly when displaying second precision,
# you may want to set this to something smaller, like 1000 or 500.
clock_update_interval: 60000

# Font size to use for the clock:
clock_font_size: 170

# Font size to use for the labels of launcher items:
launcher_font_size: 40

# Font to use (TrueType font, or anything else supported by `rusttype`):
font: /usr/share/fonts/ttf-dejavu/DejaVuSansMono.ttf

# Configuration of launcher items. See below.
launcher: []

# These only have an effect when launcher items are configured. See below.
launcher_offset: 500
icon_size: 150
h_spacing: 20
v_spacing: 40
```

## Launcher items

Launcher items consist of three fields:
- `icon`: path to a PNG image to use as an icon for this item
- `label`: string to display below the icon
- `command`: command to execute when the item is pressed

**Example:**

```
launcher:
- icon: /usr/share/icons/hicolor/128x128/apps/chromium.png
  label: Chromium
  command: ["swaymsg", "exec", "--", "chromium-browser", "--start-maximized"]
```

The `command` is expected to return right away, and exit with a status of 0. Configuring a command that does not exit right away will block the launcher until the command exits (which is fine, if you don't want to interact with the launcher while the application is running).
If a command exits with non-zero status, the launcher currently logs an error and exits (If the launcher was started via sway, the output can usually be found in `~/.xsession-errors`).


### Layout

Items are layed out in a grid, starting at the top-right.

There are four options that control the layout of launcher items:
- `launcher_offset`: this is the offset from the top of the screen to the first row of the grid. This should be at least least `1.5 * clock_font_size`, so the clock does not overlap the icons.
- `icon_size`: size of icons in pixels. Must match the size of the images provided via the `icon` field of each launcher item
- `h_spacing`: horizontal spacing between items
- `v_spacing`: vertical spacing between items. the labels are drawn in this space, so you may want to add `launcher_font_size` to this value


# TODO

- [ ] Add SVG support
- [ ] Generate launcher icons from `.desktop` files
- [ ] More advanced layout options

